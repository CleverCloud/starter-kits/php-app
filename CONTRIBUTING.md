# Requirements

> WIP :construction:

- use this https://gitmoji.carloscuesta.me/ for your commit messages
- never use the `:hankey:` emoji
- make a merge request
- "avoid" commit directly on `master` branch
- use `[skip ci]` in the commit message if you don't need to deploy the application
